from math import trunc
from random import randint

def inverseAlpharandom(character):
    if(character.isnumeric()):
        return int(character)
    return ord(character) - 97

def polibioDescipher(word):
    first = inverseAlpharandom(word[0])
    second = inverseAlpharandom(word[1])
    third = inverseAlpharandom(word[2])
    return '{}'.format(chr(first * 32 + second * 8 + third -1))
    

if __name__ == '__main__':
    message = input('Message to descipher: ')
    message = message.lower().replace(" ", "")
    messagecallback = ''
    if len(message) % 3 == 0:
        while message != '':
            letter = message[:3]
            message = message[3:]
            messagecallback += polibioDescipher(letter)
        print(messagecallback)
    else:
        print('Corrupted data')