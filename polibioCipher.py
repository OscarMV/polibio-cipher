################################################################################
# ----------------------------------- ESCOM ---------------------------------- #
# ---------------------- Autor: Oscar Martinez Vazquez ----------------------- #
# ----------------------- Escuela Superior de computo ------------------------ #
# ------------------------------ Primavera 2020 ------------------------------ #
# ---------------- Cifrado, variacion del cuadrado de Polibio ---------------- #
################################################################################

from random import randint

def alpharandom(number):
    if(randint(0, 1)):
        return chr(number + 97)
    return number

def polibioCipher(word):
    wordnum = ord(word) + 1
    first = wordnum//32
    wordnum = wordnum % 32
    second = wordnum // 8
    third = wordnum % 8
    first = alpharandom(first)
    second = alpharandom(second)
    third = alpharandom(third)
    return '{}{}{}'.format(first, second, third)

if __name__ == '__main__':
    message = input('Message to cipher: ')
    messagecallback = ''
    for letter in message:
        messagecallback += polibioCipher(letter)
    print(messagecallback)